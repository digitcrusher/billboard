#include <cassert>
#include <cmath>
#include <iostream>
#include <list>
#include <memory>

#define SFML_STATIC
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <chipmunk/chipmunk.h>

using namespace std;

float const PI = 3.14159265358;

template<typename T>
T dist_to(sf::Vector2<T> a, sf::Vector2<T> b) {
  return hypot(a.x - b.x, a.y - b.y);
}
template<typename T>
T angle_to(sf::Vector2<T> a, sf::Vector2<T> b) {
  return atan2(b.y - a.y, b.x - a.x);
}

bool is_point_above_line(sf::Vector2f point, sf::Vector2f a, sf::Vector2f b) {
  float dy = b.y - a.y;
  float dx = b.x - a.x;
  return point.y < (point.x - a.x) * dy / dx + a.y;
}
bool is_point_below_line(sf::Vector2f point, sf::Vector2f a, sf::Vector2f b) {
  float dy = b.y - a.y;
  float dx = b.x - a.x;
  return point.y > (point.x - a.x) * dy / dx + a.y;
}

cpVect to_cpv(sf::Vector2f vec) {
  return cpv(vec.x, vec.y);
}
sf::Vector2f to_sf(cpVect vec) {
  return sf::Vector2f(vec.x, vec.y);
}



// All dimensions are in centimetres.

class Bill;
class BillboardScene;
class Scene;
class Game;

class Scene {
public:
  Game *game;
  virtual void update_pre_input(float delta_time) {}
  virtual void handle_event(sf::Event event, float delta_time) {}
  virtual void update_post_input(float delta_time) {}
  virtual void draw(float delta_time) {}
};

class BillboardObject {
public:
  BillboardScene *scene;
  BillboardObject(BillboardScene *scene): scene(scene) {}
  virtual void update(float delta_time) {}
  virtual void draw(float delta_time) {}
};

enum class BillType {
  SOLID, STRIPED, CUE, EIGHT
};

string bill_type_name(BillType type) {
  if(type == BillType::SOLID) return "solid";
  if(type == BillType::STRIPED) return "striped";
  if(type == BillType::CUE) return "cue";
  if(type == BillType::EIGHT) return "eight";
  __builtin_unreachable();
}

float const BILL_RADIUS = 2.85;
float const BILL_RESTITUTION = 0.7;
float const BILL_OUTLINE_THICKNESS = 2.0;
float const BOARD_FRICTION = 0.7;

namespace BillColors {
  auto const YELLOW   = sf::Color(255, 255,   0, 255),
             BLUE     = sf::Color(  0,   0, 255, 255),
             RED      = sf::Color(255,   0,   0, 255),
             PURPLE   = sf::Color(127,   0, 127, 255),
             ORANGE   = sf::Color(255, 127,   0, 255),
             GREEN    = sf::Color(  0, 255,   0, 255),
             BURGUNDY = sf::Color(127,   0,  32, 255),
             CUE      = sf::Color(255, 255, 255, 255),
             EIGHT    = sf::Color(  0,   0,   0, 255);
};

class Bill: public BillboardObject {
public:
  BillType type;
  float radius;
  sf::Color color;

  cpBody *cp_body;
  cpShape *cp_shape;

  bool is_hidden = false;

  Bill(BillboardScene *scene, BillType type, float radius, sf::Color color, sf::Vector2f pos);
  ~Bill();
  void update(float delta_time);
  void draw(float delta_time);
  sf::Vector2f pos();
  sf::Vector2f vel();
  void set_vel(sf::Vector2f pos);
  void set_pos(sf::Vector2f pos);
};

float const BOARD_WIDTH = 236.0;
float const BOARD_HEIGHT = 137.0;
float const BOARD_WALL_RESTITUTION = 1.0;
float const BOARD_WALL_FRICTION = 1.0;

class Board: public BillboardObject {
public:
  float width = BOARD_WIDTH, height = BOARD_HEIGHT;
  sf::Color color = sf::Color(96, 32, 0, 255);
  float left, right, top, bottom;

  float bed_width = 198.0, bed_height = 99.0;
  sf::Color bed_color = sf::Color(16, 112, 75, 255);
  float bed_left, bed_right, bed_top, bed_bottom;

  float pocket_width = 12.05;
  float pocket_depth = 5.1;
  float hole_depth = 10.0;
  sf::Color hole_color = sf::Color::Black;

  sf::Vector2f side_pocket_tl, side_pocket_tr;
  sf::Vector2f side_pocket_ml, side_pocket_mr;
  sf::Vector2f side_pocket_bl, side_pocket_br;

  sf::Vector2f corner_pocket_tl, corner_pocket_tr;
  sf::Vector2f corner_pocket_ml, corner_pocket_mr;
  sf::Vector2f corner_pocket_bl, corner_pocket_br;

  Board(BillboardScene *scene);
  void update(float delta_time);
  void draw(float delta_time);
  bool is_bill_out(shared_ptr<Bill> bill);
};

enum class BillboardState {
  READY, WAIT, DONE_WON, DONE_LOST
};
enum class SelectionType {
  NONE, PREAIM, AIM, MOVE
};
enum class Player {
  A, B
};
enum class PlayerBillType {
  NONE, SOLID, STRIPED
};

Player next_player(Player player) {
  return player == Player::A ? Player::B : Player::A;
}

string player_name(Player player) {
  return player == Player::A ? "A" : "b";
}

PlayerBillType to_player_bill_type(BillType type) {
  if(type == BillType::SOLID) return PlayerBillType::SOLID;
  if(type == BillType::STRIPED) return PlayerBillType::STRIPED;
  return PlayerBillType::NONE;
}

BillType to_bill_type(PlayerBillType type) {
  if(type == PlayerBillType::SOLID) return BillType::SOLID;
  if(type == PlayerBillType::STRIPED) return BillType::STRIPED;
  assert(false);
}

string player_bill_type_name(PlayerBillType type) {
  if(type == PlayerBillType::NONE) return "";
  return bill_type_name(to_bill_type(type));
}

PlayerBillType other_player_bill_type(PlayerBillType type) {
  if(type == PlayerBillType::NONE) return PlayerBillType::NONE;
  if(type == PlayerBillType::SOLID) return PlayerBillType::STRIPED;
  if(type == PlayerBillType::STRIPED) return PlayerBillType::SOLID;
  __builtin_unreachable();
}

struct TriangleBill {
  int x, y;
  BillType type;
  sf::Color color;
};
vector<TriangleBill> const TRIANGLE_BILLS = {
  {0, 0, BillType::SOLID, BillColors::RED},

  {-1, -1, BillType::STRIPED, BillColors::YELLOW},
  {-1, 1, BillType::STRIPED, BillColors::GREEN},

  {-2, -2, BillType::SOLID, BillColors::BURGUNDY},
  {-2, 0, BillType::EIGHT, BillColors::EIGHT},
  {-2, 2, BillType::SOLID, BillColors::ORANGE},

  {-3, -3, BillType::STRIPED, BillColors::PURPLE},
  {-3, -1, BillType::SOLID, BillColors::GREEN},
  {-3, 1, BillType::STRIPED, BillColors::BLUE},
  {-3, 3, BillType::STRIPED, BillColors::BURGUNDY},

  {-4, -4, BillType::SOLID, BillColors::YELLOW},
  {-4, -2, BillType::STRIPED, BillColors::RED},
  {-4, 0, BillType::SOLID, BillColors::PURPLE},
  {-4, 2, BillType::STRIPED, BillColors::ORANGE},
  {-4, 4, BillType::SOLID, BillColors::BLUE}
};

float const MIN_HIT_FORCE = BILL_RADIUS * 10.0;
float const MAX_HIT_FORCE = MIN_HIT_FORCE * 10.0;
float const MAX_HIT_FORCE_DIST = 20.0;
float const MAX_BILL_VEL_FOR_NEXT_TURN = 2.0;
float const BILL_GUIDE_WIDTH = 1.0;
sf::Color const BILL_GUIDE_COLOR = sf::Color(0, 0, 0, 127);
float const GAME_STATUS_TEXT_SIZE = 12.0;
sf::Color const GAME_STATUS_TEXT_COLOR = sf::Color(255, 255, 255, 255);

class BillboardScene: public Scene {
public:
  float time;

  cpSpace *cp_space;

  list<shared_ptr<Bill>> bills;
  shared_ptr<Board> board;

  SelectionType selection_type = SelectionType::NONE;
  shared_ptr<Bill> selected_bill;

  BillboardState state = BillboardState::READY;
  Player curr_player;
  PlayerBillType player_a_bill_type;
  PlayerBillType player_b_bill_type;
  bool has_faul;
  bool did_player_score;

  BillboardScene();
  ~BillboardScene();

  void add_bill(shared_ptr<Bill> bill) {
    this->bills.push_back(bill);
  }

  void update_pre_input(float delta_time);
  void handle_event(sf::Event event, float delta_time);
  void update_post_input(float delta_time);
  void draw(float delta_time);
};

int const WINDOW_WIDTH = BOARD_WIDTH * 2;
int const WINDOW_HEIGHT = BOARD_HEIGHT * 2;
string const PRIMARY_FONT = "comic.ttf";

class Game {
public:
  unique_ptr<sf::RenderWindow> window;
  bool is_running = true;
  float ups = 60.0, dps = 60.0;
  shared_ptr<Scene> scene;

  shared_ptr<BillboardScene> billboard_scene;
  sf::Font primary_font;

  Game();

  void set_scene(shared_ptr<Scene> scene);

  void handle_events(float delta_time);
  void update(float delta_time);
  void draw(float delta_time);

  void loop();
};



Bill::Bill(BillboardScene *scene, BillType type, float radius, sf::Color color, sf::Vector2f pos):
  BillboardObject(scene), type(type), radius(radius), color(color) {

  float mass = pow(this->radius, 2) * PI;
  float moment = cpMomentForCircle(mass, 0.0, this->radius * 2.0, cpvzero);
  this->cp_body = cpBodyNew(mass, moment);

  cpBodySetPosition(this->cp_body, to_cpv(pos));
  cpSpaceAddBody(this->scene->cp_space, this->cp_body);

  this->cp_shape = cpCircleShapeNew(this->cp_body, this->radius, cpvzero);

  cpShapeSetElasticity(this->cp_shape, BILL_RESTITUTION);
  cpShapeSetFriction(this->cp_shape, 0.0);
  cpSpaceAddShape(this->scene->cp_space, this->cp_shape);
}
Bill::~Bill() {
  cpShapeFree(this->cp_shape);
  cpBodyFree(this->cp_body);
}
void Bill::update(float delta_time) {
  this->set_vel(this->vel() * float(1.0 - BOARD_FRICTION * delta_time));
}
void Bill::draw(float delta_time) {
  if(this->is_hidden) return;

  sf::CircleShape shape(this->radius);
  shape.setPosition(this->pos() - sf::Vector2f(this->radius, this->radius));

  if(this->type == BillType::SOLID) {
    shape.setFillColor(this->color);

  } else if(this->type == BillType::STRIPED) {
    shape.setRadius(this->radius / 2.0);
    shape.setPosition(this->pos() - sf::Vector2f(this->radius, this->radius) / 2.0f);

    shape.setFillColor(sf::Color::White);
    shape.setOutlineColor(this->color);
    shape.setOutlineThickness(this->radius / 2.0);

  } else if(this->type == BillType::CUE) {
    shape.setFillColor(BillColors::CUE);

  } else if(this->type == BillType::EIGHT) {
    shape.setFillColor(BillColors::EIGHT);
  }

  this->scene->game->window->draw(shape);

  if(this->scene->selected_bill.get() == this) {
    sf::CircleShape outline(this->radius);
    outline.setPosition(this->pos() - sf::Vector2f(this->radius, this->radius));

    outline.setFillColor(sf::Color::Transparent);
    outline.setOutlineColor(sf::Color(0, 0, 0, 127));
    outline.setOutlineThickness(BILL_OUTLINE_THICKNESS);

    this->scene->game->window->draw(outline);
  }
}
sf::Vector2f Bill::pos() {
  cpVect cp_pos = cpBodyGetPosition(this->cp_body);
  return sf::Vector2f(cp_pos.x, cp_pos.y);
}
sf::Vector2f Bill::vel() {
  cpVect cp_vel = cpBodyGetVelocity(this->cp_body);
  return sf::Vector2f(cp_vel.x, cp_vel.y);
}
void Bill::set_vel(sf::Vector2f vel) {
  cpBodySetVelocity(this->cp_body, to_cpv(vel));
}
void Bill::set_pos(sf::Vector2f pos) {
  cpBodySetPosition(this->cp_body, to_cpv(pos));
}



Board::Board(BillboardScene *scene): BillboardObject(scene) {
  this->left = -this->width / 2.0, this->right = this->width / 2.0;
  this->top = -this->height / 2.0, this->bottom = this->height / 2.0;

  this->bed_left = -this->bed_width / 2.0, this->bed_right = this->bed_width / 2.0;
  this->bed_top = -this->bed_height / 2.0, this->bed_bottom = this->bed_height / 2.0;

  this->side_pocket_tl = sf::Vector2f(-this->pocket_width / 2.0, this->bed_bottom);
  this->side_pocket_tr = sf::Vector2f( this->pocket_width / 2.0, this->bed_bottom);
  this->side_pocket_ml = this->side_pocket_tl + sf::Vector2f(0.0, this->pocket_depth);
  this->side_pocket_mr = this->side_pocket_tr + sf::Vector2f(0.0, this->pocket_depth);
  this->side_pocket_bl = this->side_pocket_ml + sf::Vector2f(0.0, this->hole_depth);
  this->side_pocket_br = this->side_pocket_mr + sf::Vector2f(0.0, this->hole_depth);

  this->corner_pocket_tl = sf::Vector2f(this->bed_right - this->pocket_width / sqrt(2), this->bed_bottom);
  this->corner_pocket_tr = sf::Vector2f(this->bed_right, this->bed_bottom - this->pocket_width / sqrt(2));
  this->corner_pocket_ml = this->corner_pocket_tl + sf::Vector2f(this->pocket_depth / sqrt(2), this->pocket_depth / sqrt(2));
  this->corner_pocket_mr = this->corner_pocket_tr + sf::Vector2f(this->pocket_depth / sqrt(2), this->pocket_depth / sqrt(2));
  this->corner_pocket_bl = this->corner_pocket_ml + sf::Vector2f(this->hole_depth / sqrt(2), this->hole_depth / sqrt(2));
  this->corner_pocket_br = this->corner_pocket_mr + sf::Vector2f(this->hole_depth / sqrt(2), this->hole_depth / sqrt(2));


  sf::Vector2f first_vert;
  sf::Vector2f last_vert;
  auto put_vert = [&](sf::Vector2f vert) {
    if(last_vert != sf::Vector2f()) {
      cpBody *static_body = cpSpaceGetStaticBody(this->scene->cp_space);
      cpShape *wall = cpSegmentShapeNew(static_body, to_cpv(last_vert), to_cpv(vert), 0.0);
      cpShapeSetElasticity(wall, BOARD_WALL_RESTITUTION);
      cpShapeSetFriction(wall, BOARD_WALL_FRICTION);
      cpSpaceAddShape(this->scene->cp_space, wall);
    }
    last_vert = vert;
    if(first_vert == sf::Vector2f()) {
      first_vert = vert;
    }
  };
  auto put_verts = [&](vector<sf::Vector2f> verts, sf::Vector2f mul) {
    if(mul.x * mul.y < 0) {
      reverse(verts.begin(), verts.end());
    }
    for(auto vert: verts) {
      vert.x *= mul.x;
      vert.y *= mul.y;
      put_vert(vert);
    }
  };

  auto put_side_pocket = [&](sf::Vector2f mul) {
    put_verts({side_pocket_tl, side_pocket_bl, side_pocket_br, side_pocket_tr}, mul);
  };
  auto put_corner_pocket = [&](sf::Vector2f mul) {
    put_verts({corner_pocket_tl, corner_pocket_ml, corner_pocket_bl,
               corner_pocket_br, corner_pocket_mr, corner_pocket_tr}, mul);
  };

  put_side_pocket({1, 1});
  put_corner_pocket({1, 1});
  put_corner_pocket({1, -1});
  put_side_pocket({1, -1});
  put_corner_pocket({-1, -1});
  put_corner_pocket({-1, 1});

  put_vert(first_vert);
}
void Board::update(float delta_time) {}
void Board::draw(float delta_time) {
  sf::RectangleShape board_shape(sf::Vector2f(this->width, this->height));
  board_shape.setFillColor(this->color);
  board_shape.setPosition(sf::Vector2f(this->left, this->top));
  this->scene->game->window->draw(board_shape);

  sf::RectangleShape bed_shape(sf::Vector2f(this->bed_width, this->bed_height));
  bed_shape.setFillColor(this->bed_color);
  bed_shape.setPosition(-sf::Vector2f(this->bed_width, this->bed_height) / 2.0f);
  this->scene->game->window->draw(bed_shape);

  sf::ConvexShape side_pocket(4);
  side_pocket.setPoint(0, this->side_pocket_tl);
  side_pocket.setPoint(1, this->side_pocket_ml);
  side_pocket.setPoint(2, this->side_pocket_mr);
  side_pocket.setPoint(3, this->side_pocket_tr);
  side_pocket.setFillColor(this->bed_color);

  sf::ConvexShape side_pocket_hole(4);
  side_pocket_hole.setPoint(0, this->side_pocket_ml);
  side_pocket_hole.setPoint(1, this->side_pocket_bl);
  side_pocket_hole.setPoint(2, this->side_pocket_br);
  side_pocket_hole.setPoint(3, this->side_pocket_mr);
  side_pocket_hole.setFillColor(this->hole_color);

  auto put_side_pocket = [&](sf::Vector2f mul) {
    side_pocket.setScale(mul);
    side_pocket_hole.setScale(mul);
    this->scene->game->window->draw(side_pocket);
    this->scene->game->window->draw(side_pocket_hole);
  };

  sf::ConvexShape corner_pocket(4);
  corner_pocket.setPoint(0, this->corner_pocket_tl);
  corner_pocket.setPoint(1, this->corner_pocket_ml);
  corner_pocket.setPoint(2, this->corner_pocket_mr);
  corner_pocket.setPoint(3, this->corner_pocket_tr);
  corner_pocket.setFillColor(this->bed_color);

  sf::ConvexShape corner_pocket_hole(4);
  corner_pocket_hole.setPoint(0, this->corner_pocket_ml);
  corner_pocket_hole.setPoint(1, this->corner_pocket_bl);
  corner_pocket_hole.setPoint(2, this->corner_pocket_br);
  corner_pocket_hole.setPoint(3, this->corner_pocket_mr);
  corner_pocket_hole.setFillColor(this->hole_color);

  auto put_corner_pocket = [&](sf::Vector2f mul) {
    corner_pocket.setScale(mul);
    corner_pocket_hole.setScale(mul);
    this->scene->game->window->draw(corner_pocket);
    this->scene->game->window->draw(corner_pocket_hole);
  };

  put_side_pocket({1, 1});
  put_corner_pocket({1, 1});
  put_corner_pocket({1, -1});
  put_side_pocket({1, -1});
  put_corner_pocket({-1, -1});
  put_corner_pocket({-1, 1});
}
bool Board::is_bill_out(shared_ptr<Bill> bill) {
  vector<pair<pair<sf::Vector2f, sf::Vector2f>, bool>> halfplanes;

  auto put = [&](sf::Vector2f a, sf::Vector2f b, sf::Vector2f mul) {
    a.x *= mul.x;
    a.y *= mul.y;
    b.x *= mul.x;
    b.y *= mul.y;
    halfplanes.push_back({{a, b}, mul.y > 0.0});
  };
  auto put_side_pocket = [&](sf::Vector2f mul) {
    put(side_pocket_ml, side_pocket_mr, mul);
  };
  auto put_corner_pocket = [&](sf::Vector2f mul) {
    put(corner_pocket_ml, corner_pocket_mr, mul);
  };

  put_side_pocket({1, 1});
  put_corner_pocket({1, 1});
  put_corner_pocket({1, -1});
  put_side_pocket({1, -1});
  put_corner_pocket({-1, -1});
  put_corner_pocket({-1, 1});

  for(auto [pts, is_below]: halfplanes) {
    auto [a, b] = pts;
    if(is_below && is_point_below_line(bill->pos(), a, b)) {
      return true;
    } else if(!is_below && is_point_above_line(bill->pos(), a, b)) {
      return true;
    }
  }
  return false;
}



BillboardScene::BillboardScene() {
  this->cp_space = cpSpaceNew();

  this->board = make_shared<Board>(this);

  auto pyramid_spot = sf::Vector2f(-this->board->width / 4.0, 0.0);
  float x_advance = cos(30.0 / 180.0 * PI) * BILL_RADIUS * 2.0;
  float y_advance = sin(30.0 / 180.0 * PI) * BILL_RADIUS * 2.0;

  for(auto bill: TRIANGLE_BILLS) {
    this->add_bill(make_shared<Bill>(
      this, bill.type, BILL_RADIUS, bill.color, sf::Vector2f(x_advance * bill.x, y_advance * bill.y) + pyramid_spot
    ));
  }

  this->add_bill(make_shared<Bill>(
    this, BillType::CUE, BILL_RADIUS, sf::Color::Black, sf::Vector2f(this->board->width / 4.0, 0.0)
  ));
}
BillboardScene::~BillboardScene() {
  cpSpaceFree(this->cp_space);
}

void BillboardScene::update_pre_input(float delta_time) {
  this->time += delta_time;
}
void BillboardScene::handle_event(sf::Event event, float delta_time) {
  switch(event.type) {
  case sf::Event::MouseButtonPressed: {
    sf::Vector2f mouse_pos = this->game->window->mapPixelToCoords(
      sf::Vector2i(event.mouseButton.x, event.mouseButton.y)
    );

    if(this->state == BillboardState::READY) {
      shared_ptr<Bill> new_selection = nullptr;
      for(auto bill: bills) {
        if(bill->type == BillType::CUE && dist_to(bill->pos(), mouse_pos) <= bill->radius) {
          new_selection = bill;
          if(new_selection != this->selected_bill) {
            break;
          }
        }
      }

      if(event.mouseButton.button == sf::Mouse::Left) {
        if(new_selection == this->selected_bill && this->selection_type == SelectionType::AIM) {
          this->selection_type = SelectionType::NONE;
          this->selected_bill = nullptr;
        } else if(new_selection != nullptr) {
          this->selection_type = SelectionType::PREAIM;
          this->selected_bill = new_selection;
        }
      } else if(event.mouseButton.button == sf::Mouse::Right) {
        if(this->has_faul && new_selection != nullptr) {
          this->selection_type = SelectionType::MOVE;
          this->selected_bill = new_selection;
        }
      }
    }
  } break;

  case sf::Event::MouseButtonReleased:
    if(event.mouseButton.button == sf::Mouse::Left) {
      if(this->selection_type == SelectionType::PREAIM) {
        this->selection_type = SelectionType::AIM;

      } else if(this->selection_type == SelectionType::AIM) {
        sf::Vector2f mouse_pos = this->game->window->mapPixelToCoords(sf::Mouse::getPosition(*this->game->window.get()));
        float mouse_dist_to_bill = dist_to(this->selected_bill->pos(), mouse_pos);
        float force = MIN_HIT_FORCE + (MAX_HIT_FORCE - MIN_HIT_FORCE) *
                      min(1.0f, (mouse_dist_to_bill - this->selected_bill->radius) / MAX_HIT_FORCE_DIST);
        auto impulse = (this->selected_bill->pos() - mouse_pos) / mouse_dist_to_bill * force;

        auto cp_curr_vel = cpBodyGetVelocity(this->selected_bill->cp_body);
        cpBodySetVelocity(this->selected_bill->cp_body, cpvadd(cp_curr_vel, to_cpv(impulse)));

        this->selection_type = SelectionType::NONE;
        this->selected_bill = nullptr;
        this->state = BillboardState::WAIT;
        this->has_faul = false;
      }

    } else if(event.mouseButton.button == sf::Mouse::Right) {
      if(this->selection_type == SelectionType::MOVE) {
        this->selection_type = SelectionType::NONE;
        this->selected_bill = nullptr;
      }
    }
    break;
  }
}
void BillboardScene::update_post_input(float delta_time) {
  if(this->selection_type == SelectionType::MOVE) {
    sf::Vector2f mouse_pos = this->game->window->mapPixelToCoords(sf::Mouse::getPosition(*this->game->window.get()));
    float x = this->board->width / 4.0 * (mouse_pos.x < 0.0 ? -1.0 : 1.0);
    float y = clamp(
      mouse_pos.y,
      this->board->bed_top + this->selected_bill->radius,
      this->board->bed_bottom - this->selected_bill->radius
    );
    this->selected_bill->set_pos(sf::Vector2f(x, y));
  }

  cpSpaceStep(this->cp_space, delta_time);

  this->board->update(delta_time);

  for(auto bill: this->bills) {
    bill->update(delta_time);

    if(bill->is_hidden) continue;

    bool is_bill_out = this->board->is_bill_out(bill);
    if(!is_bill_out) continue;

    bill->is_hidden = true;
    bill->set_vel(sf::Vector2f(0.0, 0.0));
    bill->set_pos(sf::Vector2f(this->board->width + 2137.0, this->board->height + 2137.0));

    if(bill == this->selected_bill) {
      this->selection_type = SelectionType::NONE;
      this->selected_bill = nullptr;
    }

    if(bill->type == BillType::CUE) {
      this->has_faul = true;
      // reset the cue bill
      if(this->state == BillboardState::READY) {
        this->state = BillboardState::WAIT;
        this->did_player_score = true;
      }

    } else if(bill->type == BillType::EIGHT) {
      this->state = BillboardState::DONE_WON;
      for(auto bill: this->bills) {
        if(!bill->is_hidden && bill->type == to_bill_type(this->curr_player == Player::A ? player_a_bill_type : player_b_bill_type)) {
          this->state = BillboardState::DONE_LOST;
          break;
        }
      }

    } else {
      switch(this->curr_player) {
      case Player::A:
        if(this->player_a_bill_type == PlayerBillType::NONE) {
          this->player_a_bill_type = to_player_bill_type(bill->type);
          this->player_b_bill_type = other_player_bill_type(this->player_a_bill_type);
        }
        if(bill->type == to_bill_type(this->player_a_bill_type)) {
          this->did_player_score = true;
        }
        break;
      case Player::B:
        if(this->player_b_bill_type == PlayerBillType::NONE) {
          this->player_b_bill_type = to_player_bill_type(bill->type);
          this->player_a_bill_type = other_player_bill_type(this->player_b_bill_type);
        }
        if(bill->type == to_bill_type(this->player_b_bill_type)) {
          this->did_player_score = true;
        }
        break;
      }
    }
  }

  if(this->state == BillboardState::WAIT) {
    bool can_continue = true;
    for(auto bill: this->bills) {
      if(dist_to(bill->vel(), sf::Vector2f(0.0, 0.0)) > MAX_BILL_VEL_FOR_NEXT_TURN) {
        can_continue = false;
        break;
      }
    }

    if(can_continue) {
      this->state = BillboardState::READY;
      if(!this->did_player_score) {
        this->curr_player = next_player(this->curr_player);
      }
      this->did_player_score = false;

      for(auto bill: this->bills) {
        if(bill->type == BillType::CUE && bill->is_hidden) {
          bill->set_pos(sf::Vector2f(0.0, 0.0));
          bill->is_hidden = false;
        }
      }
    }
  }
}
void BillboardScene::draw(float delta_time) {
  this->game->window->setView(sf::View(sf::Vector2f(), sf::Vector2f(this->board->width, this->board->height)));

  this->board->draw(delta_time);
  for(auto bill: this->bills) {
    bill->draw(delta_time);
  }

  if(this->selection_type == SelectionType::PREAIM || this->selection_type == SelectionType::AIM) {
    sf::Vector2f mouse_pos = this->game->window->mapPixelToCoords(sf::Mouse::getPosition(*this->game->window.get()));
    sf::Vector2f pos_offset = (mouse_pos - this->selected_bill->pos()) / dist_to(this->selected_bill->pos(), mouse_pos) * this->selected_bill->radius;

    sf::RectangleShape bill_guide(sf::Vector2f(MAX_HIT_FORCE_DIST, BILL_GUIDE_WIDTH));
    bill_guide.setPosition(this->selected_bill->pos() + pos_offset);
    bill_guide.setOrigin(sf::Vector2f(0.0, BILL_GUIDE_WIDTH / 2.0));
    bill_guide.setRotation(angle_to(this->selected_bill->pos(), mouse_pos) / PI * 180.0);
    bill_guide.setFillColor(BILL_GUIDE_COLOR);
    this->game->window->draw(bill_guide);
  }

  sf::Text game_status_text;
  game_status_text.setCharacterSize(GAME_STATUS_TEXT_SIZE);
  game_status_text.setFillColor(GAME_STATUS_TEXT_COLOR);
  game_status_text.setFont(this->game->primary_font);

  switch(this->state) {
  case BillboardState::READY:
    game_status_text.setString(
      string("Player ") + player_name(this->curr_player) + " to make a turn..." + (this->has_faul ? " with a faul" : "")
    );
    break;
  case BillboardState::WAIT:
    game_status_text.setString(
      string("Player ") + player_name(this->curr_player) + " maked a turn" + (this->has_faul ? " with a faul" : "")
    );
    break;
  case BillboardState::DONE_WON:
    game_status_text.setString(
      string("Ha Ha Player ") + player_name(this->curr_player) + " is the winer!"
    );
    break;
  case BillboardState::DONE_LOST:
    game_status_text.setString(
      string("Ha Ha Player ") + player_name(this->curr_player) + " is the looser!"
    );
    break;
  }
  game_status_text.setPosition(sf::Vector2f(this->board->left, this->board->top));
  this->game->window->draw(game_status_text);

  string color;
  switch(this->curr_player) {
  case Player::A:
    color = player_bill_type_name(this->player_a_bill_type);
    break;
  case Player::B:
    color = player_bill_type_name(this->player_b_bill_type);
    break;
  }
  if(color != "") {
    game_status_text.setString(
      string("Player ") + player_name(this->curr_player) + " has bill of color " + color
    );
    game_status_text.setPosition(
      sf::Vector2f(this->board->left, this->board->bottom - GAME_STATUS_TEXT_SIZE)
    );
    this->game->window->draw(game_status_text);
  }
}



Game::Game():
  window(make_unique<sf::RenderWindow>(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "billboard")) {

  this->window->setVerticalSyncEnabled(true);

  this->billboard_scene = make_shared<BillboardScene>();
  this->set_scene(this->billboard_scene);

  if(!this->primary_font.loadFromFile(PRIMARY_FONT)) {
    exit(EXIT_FAILURE);
  }
}

void Game::set_scene(shared_ptr<Scene> scene) {
  this->scene = scene;
  scene->game = this;
}

void Game::handle_events(float delta_time) {
  sf::Event event;
  while(this->window->pollEvent(event)) {
    if(event.type == sf::Event::Closed) {
      this->is_running = false;
    }
    this->scene->handle_event(event, delta_time);
  }
}
void Game::update(float delta_time) {
  this->scene->update_pre_input(delta_time);
  this->handle_events(delta_time);
  this->scene->update_post_input(delta_time);
}
void Game::draw(float delta_time) {
  this->window->clear(sf::Color::Black);
  this->scene->draw(delta_time);
  this->window->display();
}

void Game::loop() {
  sf::Clock clock;
  float last_update = clock.getElapsedTime().asSeconds();
  float last_draw = clock.getElapsedTime().asSeconds();
  while(this->is_running) {
    float now = clock.getElapsedTime().asSeconds();

    if(now - last_update >= 1.0 / this->ups) {
      this->update(1.0 / this->ups);
      last_update += 1.0 / this->ups;
    }

    if(now - last_draw >= 1.0 / this->dps) {
      this->draw(now - last_draw);
      last_draw = now;
    }
  }
}

int main() {
  Game game;
  game.loop();
}
