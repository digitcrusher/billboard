#!/bin/sh

FLAGS="-std=c++17 -lsfml-graphics-s -lsfml-window-s -lsfml-system-s -lchipmunk -no-pie"

if [ "$1" = "default" ]; then
  ARCH="$(g++ -dumpmachine)"
  CPP="g++"
  FLAGS="${FLAGS} -ludev -lfreetype -lpng -lz -lXrandr -lXcursor -lxcb -lX11 -lpthread -ldl"
elif [ "$1" = "alpine" ]; then
  ARCH="i586-alpine-linux-musl"
  CPP="${ARCH}-g++"
elif [ "$1" = "windows" ]; then
  ARCH="i686-w64-mingw32"
  CPP="${ARCH}-g++"
  FLAGS="${FLAGS} -static -lopengl32 -lfreetype -lwinmm -lgdi32"
elif [ "$1" = "windows64" ]; then
  ARCH="x86_64-w64-mingw32"
  CPP="${ARCH}-g++"
  FLAGS="${FLAGS} -static -lopengl32 -lfreetype -lwinmm -lgdi32"
else
  echo "No target system given"
  exit 1
fi

FLAGS="${FLAGS} -I../external/include/${ARCH} -L../external/lib/${ARCH}"

rm -r build
mkdir build

cd src

$CPP billboard.cpp -o ../build/billboard $FLAGS || exit 1

cd ..

cp -r res/all/* build/
if [ "$(ls -A res/"${ARCH}"/)" ]; then
  cp -r res/"${ARCH}"/* build/
fi
